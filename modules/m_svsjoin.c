/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 1999 by the Bahamut Development Team.
 *  Copyright (C) 2011 by the Hybrid Development Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "channel_mode.h"
#include "numeric.h"
#include "s_serv.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "irc_string.h"
#include "s_user.h"
#include "hash.h"
#include "watch.h"
#include "whowas.h"


/*! \brief SVSJOIN command handler (called by services)
 *
 * \param client_p Pointer to allocated Client struct with physical connection
 *                 to this server, i.e. with an open socket connected.
 * \param source_p Pointer to allocated Client struct from which the message
 *                 originally comes from.  This can be a local or remote client.
 * \param parc     Integer holding the number of supplied arguments.
 * \param parv     Argument vector where parv[0] .. parv[parc-1] are non-NULL
 *                 pointers.
 */
static void
me_svsjoin(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  struct Client *target_p;
  struct Channel *chptr;
  unsigned int type;
  char mode, sjmode;

  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL || !MyClient(target_p))
    return;

  /* select our modes from parv[2] if they exist... (chanop) */
  switch (*parv[2])
  {
#ifdef CHANAQ
    case '~':
      type = CHFL_OWNER;
      mode = 'q';
      sjmode = CHANOWNER_PREFIX;
      parv[2]++;
      break;
    case '&':
      type = CHFL_PROTECTED;
      mode = 'a';
      sjmode = CHANPROTECT_PREFIX;
      parv[2]++;
      break;
#endif
    case '@':
      type = CHFL_CHANOP;
      mode = 'o';
      sjmode = '@';
      parv[2]++;
      break;
#ifdef HALFOPS
    case '%':
      type = CHFL_HALFOP;
      mode = 'h';
      sjmode = '%';
      parv[2]++;
      break;
#endif
    case '+':
      type = CHFL_VOICE;
      mode = 'v';
      sjmode = '+';
      parv[2]++;
      break;
    default:
      type = 0;
      mode = sjmode = 0;
  }

  chptr = hash_find_channel(parv[2]);
  if (chptr != NULL)
  {
    if(IsMember(target_p, chptr))
      return;

    add_user_to_channel(chptr, target_p, type, 0);

    sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s!%s@%s JOIN :%s",
      target_p->name, target_p->username,
      target_p->host, chptr->chname);

    if (sjmode)
      sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s MODE %s +%c %s",
          me.name, chptr->chname, mode, target_p->name);

    if (sjmode)
    {
      sendto_server(target_p, CAP_TS6, NOCAPS,
          ":%s SJOIN %lu %s + :%c%s", me.id,
          (unsigned long) chptr->channelts, chptr->chname,
          sjmode, target_p->id);
      sendto_server(target_p, NOCAPS, CAP_TS6,
          ":%s SJOIN %lu %s + :%c%s", me.name,
          (unsigned long) chptr->channelts, chptr->chname,
          sjmode, target_p->name);
    }
    else
    {
      sendto_server(target_p, CAP_TS6, NOCAPS,
          ":%s SJOIN %lu %s + :%s", me.id,
          (unsigned long) chptr->channelts, chptr->chname,
          target_p->id);
      sendto_server(target_p, NOCAPS, CAP_TS6,
          ":%s SJOIN %lu %s + :%s", me.name,
          (unsigned long) chptr->channelts, chptr->chname,
          target_p->name);
    }

    clear_invites_to(chptr, target_p);

    if (chptr->topic[0])
    {
      sendto_one(target_p, form_str(RPL_TOPIC), me.name,
                 target_p->name, chptr->chname, chptr->topic);

      sendto_one(target_p, form_str(RPL_TOPICWHOTIME),
                 me.name, target_p->name, chptr->chname,
                 chptr->topic_info, chptr->topic_time);
    }

    target_p->localClient->last_join_time = CurrentTime;
    channel_member_names(target_p, chptr, 1);
  }
  else
  {
    const char *newch = parv[2];

    if (!check_channel_name(newch,1))
      return;

    chptr = make_channel(newch);
    add_user_to_channel(chptr, target_p, CHFL_CHANOP, 0);

    /* send out a join, make target_p join chptr */
    sendto_server(target_p, CAP_TS6, NOCAPS,
          ":%s SJOIN %lu %s +nt :@%s",
          me.id, (unsigned long) chptr->channelts,
          chptr->chname, ID(target_p));
    sendto_server(target_p, NOCAPS, CAP_TS6,
          ":%s SJOIN %lu %s +nt :@%s",
          me.name, (unsigned long) chptr->channelts,
          chptr->chname, target_p->name);

    sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s!%s@%s JOIN :%s",
      target_p->name, target_p->username,
      target_p->host, chptr->chname);

    chptr->mode.mode |= MODE_TOPICLIMIT | MODE_NOPRIVMSGS;

    sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s MODE %s +nt",
        me.name, chptr->chname);

    target_p->localClient->last_join_time = CurrentTime;
    channel_member_names(target_p, chptr, 1);
  }
}

/*! \brief SVSPART command handler (called by services)
 *
 * \param client_p Pointer to allocated Client struct with physical connection
 *                 to this server, i.e. with an open socket connected.
 * \param source_p Pointer to allocated Client struct from which the message
 *                 originally comes from.  This can be a local or remote client.
 * \param parc     Integer holding the number of supplied arguments.
 * \param parv     Argument vector where parv[0] .. parv[parc-1] are non-NULL
 *                 pointers.
 */
static void
me_svspart(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  struct Client *target_p;
  struct Channel *chptr;
  struct Membership *member;

  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL || !MyClient(target_p))
    return;

  chptr = hash_find_channel(parv[2]);
  if (chptr == NULL)
    return;

  member = find_channel_link(target_p, chptr);
  if (member == NULL)
    return;

  sendto_server(target_p, CAP_TS6, NOCAPS,
      ":%s PART %s", ID(target_p), chptr->chname);
  sendto_server(target_p, NOCAPS, CAP_TS6,
      ":%s PART %s", target_p->name, chptr->chname);

  sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s!%s@%s PART %s",
      target_p->name, target_p->username, target_p->host, chptr->chname);
  remove_user_from_channel(member);
}

static struct Message svsjoin_msgtab = {
  .cmd = "SVSJOIN",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_svsjoin,
  .handlers[OPER_HANDLER] = m_ignore,
};

static struct Message svspart_msgtab = {
  .cmd = "SVSPART",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_svspart,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&svsjoin_msgtab);
  mod_add_cmd(&svspart_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&svsjoin_msgtab);
  mod_del_cmd(&svspart_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
