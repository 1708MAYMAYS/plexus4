/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_whois.c
 * \brief Includes required functions for processing the WHOIS command.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "client.h"
#include "hash.h"
#include "channel.h"
#include "channel_mode.h"
#include "ircd.h"
#include "numeric.h"
#include "conf.h"
#include "s_misc.h"
#include "s_serv.h"
#include "s_user.h"
#include "send.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"


/* whois_person()
 *
 * inputs	- source_p client to report to
 *		- target_p client to report on
 * output	- NONE
 * side effects	- 
 */
static void
whois_person(struct Client *source_p, struct Client *target_p)
{
  char buf[IRCD_BUFSIZE] = { '\0' };
  const dlink_node *lp = NULL;
  char *t = NULL;
  int cur_len = 0;
  int mlen = 0;
  int tlen = 0;
  int reply_to_send = 0;

  sendto_one(source_p, form_str(RPL_WHOISUSER), me.name,
             source_p->name, target_p->name,
             target_p->username, target_p->host, target_p->info);

  cur_len = mlen = snprintf(buf, sizeof(buf), form_str(RPL_WHOISCHANNELS),
                            me.name, source_p->name, target_p->name, "");
  t = buf + mlen;

  DLINK_FOREACH(lp, target_p->channel.head)
  {
    const struct Membership *ms = lp->data;
    int show = ShowChannel(source_p, target_p, ms->chptr);

    if (show || HasUMode(source_p, UMODE_ADMIN))
    {
      // ! + one char prefix + name + space
      if ((cur_len + 1 + 1 + strlen(ms->chptr->chname) + 1 + 1) > (IRCD_BUFSIZE - 2))
      {
        *(t - 1) = '\0';
        sendto_one(source_p, "%s", buf);
        cur_len = mlen;
        t = buf + mlen;
      }

      tlen = sprintf(t, "%s%s%s ", show ? "" : "!", get_member_status(ms, 0),
                     ms->chptr->chname);
      t += tlen;
      cur_len += tlen;
      reply_to_send = 1;
    }
  }

  if (reply_to_send)
  {
    *(t - 1) = '\0';
    sendto_one(source_p, "%s", buf);
  }

  if ((ConfigServerHide.hide_servers || IsHidden(target_p->servptr)) &&
      !(HasUMode(source_p, UMODE_OPER) || target_p == source_p))
    sendto_one(source_p, form_str(RPL_WHOISSERVER),
               me.name, source_p->name, target_p->name,
               ConfigServerHide.hidden_name,
               ServerInfo.network_desc);
  else
    sendto_one(source_p, form_str(RPL_WHOISSERVER),
               me.name, source_p->name, target_p->name,
               target_p->servptr->name, target_p->servptr->info);

  if (target_p->away[0])
    sendto_one(source_p, form_str(RPL_AWAY),
               me.name, source_p->name, target_p->name,
               target_p->away);

  if (HasUMode(target_p, UMODE_CALLERID | UMODE_SOFTCALLERID))
  {
    int notsoft = !!HasUMode(target_p, UMODE_CALLERID);

    sendto_one(source_p, form_str(RPL_TARGUMODEG),
               me.name, source_p->name, target_p->name,
               notsoft ? "+g" : "+G",
               notsoft ? "server side ignore" :
               "server side ignore with the exception of common channels");
  }

  if (HasUMode(target_p, UMODE_OPER))
  {
    buf[0] = 0;
    
    strcat(buf, "is an IRC Operator");

    if (HasFlag(target_p, FLAGS_SERVICE) || HasUMode(target_p, UMODE_SERVICE))
      strcat(buf, " - Network Service");
    else if (HasUMode(target_p, UMODE_NETADMIN))
      strcat(buf, " - Network Administrator");
    else if (HasUMode(target_p, UMODE_ADMIN))
      strcat(buf, " - Server Administrator");

    sendto_one(source_p, form_str(RPL_WHOISOPERATOR),
                 me.name, source_p->name, target_p->name, buf);
  }

  if (HasUMode(source_p, UMODE_OPER) && IsCaptured(target_p))
    sendto_one(source_p, form_str(RPL_ISCAPTURED),
        me.name, source_p->name, target_p->name);

  if (HasUMode(target_p, UMODE_REGISTERED))
  {
    if (!ConfigFileEntry.account_whois && ((target_p == source_p || HasUMode(source_p, UMODE_OPER)) && !EmptyString(target_p->suser)))
    {
      sendto_one(source_p, form_str(RPL_WHOISREGNICK),
          me.name, source_p->name, target_p->name, target_p->suser);
    }
    else
      sendto_one(source_p, form_str(RPL_WHOISREGNICK),
          me.name, source_p->name, target_p->name, "this nick");
  }

  if (ConfigFileEntry.account_whois && !EmptyString(target_p->suser))
  {
    sendto_one(source_p, form_str(RPL_WHOISACCOUNT), me.name, source_p->name, target_p->name, target_p->suser);
  }

  if (HasUMode(target_p, UMODE_SSL))
  {
    sendto_one(source_p, form_str(RPL_WHOISSECURE), me.name,
               source_p->name, target_p->name);

    if (!EmptyString(target_p->certfp))
      sendto_one(source_p, form_str(RPL_WHOISCERTFP), me.name,
                 source_p->name, target_p->name, target_p->certfp);
  }

  if (HasUMode(source_p, UMODE_OPER) && MyClient(target_p) && target_p->localClient->cgisockhost)
    sendto_one(source_p, form_str(RPL_WHOISREALIP), me.name, source_p->name, target_p->name, target_p->localClient->cgisockhost);

  if (HasUMode(source_p, UMODE_OPER) || source_p == target_p)
  {
    char ubuf[IRCD_BUFSIZE];
    char authflags[14], *p = authflags;
    struct Snomasks mask = { 0, 0 };

    send_umode(NULL, target_p, 0, &mask, ubuf);
    if(*ubuf == '\0')
      strcat(ubuf, "+");

    if(IsExemptResv(target_p))
      *p++ = '$';
    if(IsIPSpoof(target_p))
      *p++ = '=';
    if(IsExemptKline(target_p))
      *p++ = '^';
    if(IsExemptGline(target_p))
      *p++ = '_';
    if(IsExemptLimits(target_p))
      *p++ = '>';
    if(IsCanFlood(target_p))
      *p++ = '|';
    *p++ = 0;

    sendto_one(source_p, form_str(RPL_WHOISMODES),
        me.name, source_p->name, target_p->name, ubuf, *authflags ? authflags : "[none]");
  }

  {
    int hide_ip = 0;

    if (EmptyString(target_p->sockhost) || !strcmp(target_p->sockhost, "0"))
      hide_ip = 1;
    else if (IsIPSpoof(target_p) && ConfigFileEntry.hide_spoof_ips)
      hide_ip = 1;

    if (source_p == target_p || HasUMode(source_p, UMODE_OPER))
    {
      sprintf(buf, "%s@%s [%s]", target_p->username, target_p->realhost, hide_ip ? "255.255.255.255" : target_p->sockhost);
      sendto_one(source_p, form_str(RPL_WHOISACTUALLY), me.name,
          source_p->name, target_p->name, "is actually", buf);
    }

    if (ConfigFileEntry.enable_cloak_system && ConfigFileEntry.cloak_whois_actually && !IsIPSpoof(target_p))
    {
      /* Show the users's cloaked host and IP if they whois themselves and they differ */
      if((HasUMode(source_p, UMODE_OPER) || source_p == target_p) && strcmp(target_p->cloaked_ip, target_p->cloaked_host))
      {
        sprintf(buf, "%s [%s]", target_p->cloaked_host, target_p->cloaked_ip);
        sendto_one(source_p, form_str(RPL_WHOISACTUALLY), me.name,
            source_p->name, target_p->name, "has cloak", buf);

      }
      /* Show the user's cloaked IP if their current host isn't their cloaked IP */
      else if (strcmp(target_p->host, target_p->cloaked_ip))
        sendto_one(source_p, form_str(RPL_WHOISACTUALLY), me.name,
            source_p->name, target_p->name, "has cloak", target_p->cloaked_ip);
    }
  }

  if (MyConnect(target_p))
  {
    if (!HasUMode(target_p, UMODE_HIDECHANNELS) || HasUMode(source_p, UMODE_OPER) || source_p == target_p)
      sendto_one(source_p, form_str(RPL_WHOISIDLE),
               me.name, source_p->name, target_p->name,
               idle_time_get(source_p, target_p),
               target_p->localClient->firsttime);
  }

  if (HasUMode(target_p, UMODE_OPER) && target_p != source_p && HasUMode(target_p, UMODE_SPY))
    sendto_one(target_p, ":%s NOTICE %s :*** Notice -- %s (%s@%s) [%s] is doing "
                         "a whois on you", me.name, target_p->name, source_p->name,
                         source_p->username, source_p->realhost, source_p->servptr->name);
}

/* do_whois()
 *
 * inputs       - pointer to /whois source
 *              - number of parameters
 *              - pointer to parameters array
 * output       - pointer to void
 * side effects - Does whois
 */
static void
do_whois(struct Client *client_p, struct Client *source_p, const char *name)
{
  struct Client *target_p = hash_find_client(name);

  if (target_p && IsClient(target_p))
    whois_person(source_p, target_p);
  else
    sendto_one(source_p, form_str(ERR_NOSUCHNICK),
               me.name, source_p->name, name);

  sendto_one(source_p, form_str(RPL_ENDOFWHOIS),
             me.name, source_p->name, name);
}

/*
** m_whois
**      parv[0] = sender prefix
**      parv[1] = nickname masklist
*/
static void
m_whois(struct Client *client_p, struct Client *source_p,
        int parc, char *parv[])
{
  static time_t last_used = 0;

  if (parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NONICKNAMEGIVEN),
               me.name, source_p->name);
    return;
  }

  if (parc > 2 && !EmptyString(parv[2]))
  {
    /* seeing as this is going across servers, we should limit it */
    if ((last_used + ConfigFileEntry.pace_wait_simple) > CurrentTime)
    {
      sendto_one(source_p, form_str(RPL_LOAD2HI),
                 me.name, source_p->name);
      return;
    }

    last_used = CurrentTime;

    /*
     * if we have serverhide enabled, they can either ask the clients
     * server, or our server.. I dont see why they would need to ask
     * anything else for info about the client.. --fl_
     */
    if (ConfigServerHide.disable_remote_commands)
      parv[1] = parv[2];

    if (hunt_server(client_p, source_p, ":%s WHOIS %s :%s", 1,
                    parc, parv) != HUNTED_ISME)
      return;

    parv[1] = parv[2];
  }

  do_whois(client_p, source_p, parv[1]);
}

/*
** mo_whois
**      parv[0] = sender prefix
**      parv[1] = nickname masklist
*/
static void
mo_whois(struct Client *client_p, struct Client *source_p,
         int parc, char *parv[])
{
  if (parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NONICKNAMEGIVEN),
               me.name, source_p->name);
    return;
  }

  if (parc > 2 && !EmptyString(parv[2]))
  {
    if (hunt_server(client_p, source_p, ":%s WHOIS %s :%s", 1,
                    parc, parv) != HUNTED_ISME)
      return;

    parv[1] = parv[2];
  }

  do_whois(client_p, source_p, parv[1]);
}

static struct Message whois_msgtab =
{
  .cmd = "WHOIS",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_whois,
  .handlers[SERVER_HANDLER] = mo_whois,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_whois,
};

static void
module_init(void)
{
  mod_add_cmd(&whois_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&whois_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
