/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_version.c
 * \brief Includes required functions for processing the VERSION command.
 * \version $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "numeric.h"
#include "conf.h"
#include "s_serv.h"
#include "s_user.h"
#include "send.h"
#include "parse.h"
#include "modules.h"


/* Option string. */
static const char serveropts[] =
{
  'T',
  'S',
#ifdef TS_CURRENT
  '0' + TS_CURRENT,
#endif
/* ONLY do TS */
/* ALWAYS do TS_WARNINGS */
  'o',
  'w',
  '\0'
};

/*
 * m_version - VERSION command handler
 *      parv[0] = sender prefix
 *      parv[1] = remote server
 */
static void
m_version(struct Client *client_p, struct Client *source_p,
          int parc, char *parv[])
{
  static time_t last_used = 0;

  if ((last_used + ConfigFileEntry.pace_wait_simple) > CurrentTime)
  {
    /* safe enough to give this on a local connect only */
    sendto_one(source_p, form_str(RPL_LOAD2HI),
               me.name, source_p->name);
    return;
  }

  last_used = CurrentTime;

  if (!ConfigServerHide.disable_remote_commands)
    if (hunt_server(client_p, source_p, ":%s VERSION :%s",
                    1, parc, parv) != HUNTED_ISME)
      return;

  sendto_one(source_p, form_str(RPL_VERSION), me.name,
             source_p->name, ircd_version, serno,
             me.name, serveropts);
  show_isupport(source_p);
}

/*
 * mo_version - VERSION command handler
 *      parv[0] = sender prefix
 *      parv[1] = remote server
 */
static void
mo_version(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{

  if (hunt_server(client_p, source_p, ":%s VERSION :%s",
                  1, parc, parv) != HUNTED_ISME)
    return;

  sendto_one(source_p, form_str(RPL_VERSION), me.name,
             source_p->name, ircd_version, serno,
             me.name, serveropts);
  show_isupport(source_p);
}

/*
 * ms_version - VERSION command handler
 *      parv[0] = sender prefix
 *      parv[1] = remote server
 */
static void
ms_version(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  if (hunt_server(client_p, source_p, ":%s VERSION :%s",
                  1, parc, parv) != HUNTED_ISME)
    return;

  sendto_one(source_p, form_str(RPL_VERSION),
             ID_or_name(&me, client_p),
             ID_or_name(source_p, client_p),
             ircd_version, serno,
             me.name, serveropts);
  show_isupport(source_p);
}

static struct Message version_msgtab =
{
  .cmd = "VERSION",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_version,
  .handlers[SERVER_HANDLER] = ms_version,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_version,
};

static void
module_init(void)
{
  mod_add_cmd(&version_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&version_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
