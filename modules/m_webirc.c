/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2012-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_webirc.c
 * \brief Includes required functions for processing the WEBIRC command.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "client.h"
#include "ircd.h"
#include "send.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"
#include "hostmask.h"
#include "dnsbl.h"
#include "memory.h"
#include "s_user.h"
#include "listener.h"
#include "s_serv.h"
#include "hash.h"

#define CACHED_WEBIRC_MAX 32 /* unique ip + password */

static dlink_list cached_webircs;

struct webirc
{
  char ip[HOSTIPLEN + 1];
  char password[IRCD_BUFSIZE];
  dlink_node node;
};

static void
webirc_cache_clear()
{
  dlink_node *ptr, *next;

  DLINK_FOREACH_SAFE(ptr, next, cached_webircs.head)
  {
    struct webirc *w = ptr->data;

    dlinkDelete(&w->node, &cached_webircs);
    MyFree(w);
  }
}

static struct webirc *
webirc_cache_find(const char *ip, const char *password)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, cached_webircs.head)
  {
    struct webirc *w = ptr->data;

    if (!strcasecmp(ip, w->ip) && !strcmp(password, w->password))
    {
      return w;
    }
  }

  return NULL;
}

static void
webirc_cache_del(const char *ip, const char *password)
{
  struct webirc *w = webirc_cache_find(ip, password);
  if (w == NULL)
    return;

  dlinkDelete(&w->node, &cached_webircs);
  MyFree(w);
}

static void
webirc_cache_add(const char *ip, const char *password)
{
  if (webirc_cache_find(ip, password))
    return;

  if (dlink_list_length(&cached_webircs) >= CACHED_WEBIRC_MAX)
  {
    struct webirc *w = cached_webircs.head->data;
    dlinkDelete(&w->node, &cached_webircs);
    MyFree(w);
  }

  struct webirc *w = MyMalloc(sizeof(struct webirc));
  strlcpy(w->ip, ip, sizeof(w->ip));
  strlcpy(w->password, password, sizeof(w->password));
  dlinkAddTail(w, &w->node, &cached_webircs);
}

static void
webirc_broadcast(struct Client *source_p, char *parv[])
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, global_serv_list.head)
  {
    struct Client *target = ptr->data;

    if (IsMe(target) || !IsCapable(target->from, CAP_TS6) || !HasFlag(target, FLAGS_SERVICE))
      continue;

    sendto_one(target, ":%s ENCAP %s SWEBIRC REQ %s %s %s %s %s %s %s %s :%s",
                       me.id, target->name,
                       source_p->id, source_p->realhost, source_p->sockhost, source_p->certfp ? source_p->certfp : "*", "*",
                       parv[1], IsGotId(source_p) ? source_p->username : parv[2], parv[3], parv[4]);
  }
}

static void
webirc_apply(struct Client *target_p, const char *host, const char *ip)
{
  struct addrinfo hints, *res;

  assert(IsUnknown(target_p));
  assert(MyConnect(target_p));

  if (!valid_hostname(host))
    return;

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(ip, NULL, &hints, &res))
    return;

  assert(res != NULL);

  clear_dnsbl_lookup(target_p);

  /* store original host */
  if (target_p->localClient->cgisockhost == NULL)
    target_p->localClient->cgisockhost = xstrdup(target_p->sockhost);

  /* apply new ip and host */
  strlcpy(target_p->sockhost, ip, sizeof(target_p->sockhost));
  strlcpy(target_p->host, host, sizeof(target_p->sockhost));
  strlcpy(target_p->realhost, host, sizeof(target_p->realhost));

  memcpy(&target_p->localClient->ip, res->ai_addr, res->ai_addrlen);
  target_p->localClient->ip.ss_len = res->ai_addrlen;
  target_p->localClient->ip.ss.ss_family = res->ai_family;
  target_p->localClient->aftype = res->ai_family;

  freeaddrinfo(res);

  check_klines(target_p); /* this is to check dlines */
  if (IsDefunct(target_p))
    return;

  AddUMode(target_p, UMODE_WEBIRC);
  sendto_one(target_p, ":%s NOTICE %s :WEBIRC host/IP set to %s %s", me.name,
             target_p->name[0] ? target_p->name : "*", host, ip);

  /* Restart DNSBL lookups for the new IP */
  start_dnsbl_lookup(target_p);
}

/*
 * mr_webirc
 *      parv[0] = sender prefix
 *      parv[1] = password
 *      parv[2] = fake username (we ignore this)
 *      parv[3] = fake hostname
 *      parv[4] = fake ip
 */
static void
mr_webirc(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct MaskItem *conf = NULL;
  struct addrinfo hints, *res;

  assert(source_p == client_p);

  if (!client_p->localClient->listener || IsListenerServer(client_p->localClient->listener))
  {
    exit_client(source_p, &me, "Use a different port");
    return;
  }

  if (!valid_hostname(parv[3]))
  {
    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: Invalid hostname", me.name,
               source_p->name[0] ? source_p->name : "*");
    exit_client(source_p, &me, "WEBIRC: Invalid hostname");
    return;
  }

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(parv[4], NULL, &hints, &res))
  {
    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: Invalid IP %s", me.name,
               source_p->name[0] ? source_p->name : "*", parv[4]);
    exit_client(source_p, &me, "WEBIRC: Invalid IP");
    return;
  }

  assert(res != NULL);
  freeaddrinfo(res);

  conf = find_address_conf(source_p, IsGotId(source_p) ? source_p->username : "webirc", parv[1]);
  if (conf == NULL || !IsConfClient(conf) || !IsConfWebIRC(conf))
  {
    if (EmptyString(source_p->id))
    {
      const char *id;

      /* Allocate a UID. */
      while (hash_find_id((id = uid_get())) != NULL)
        ;

      strlcpy(source_p->id, id, sizeof(source_p->id));
      hash_add_id(source_p);
    }

    webirc_broadcast(source_p, parv);

    if (webirc_cache_find(source_p->sockhost, parv[1]))
    {
      // cache says okay, we'll process it ourselves until told otherwise
      webirc_apply(source_p, parv[3], parv[4]);
    }
    else
    {
      // hold registration until request comes back
      source_p->localClient->registration |= REG_NEED_WEBIRC;
    }

    return;
  }

  if (EmptyString(conf->passwd) || !match_conf_password(parv[1], conf))
  {
    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: password incorrect",
               me.name, source_p->name[0] ? source_p->name : "*");
    exit_client(source_p, &me, "WEBIRC: password incorrect");
    return;
  }

  webirc_apply(source_p, parv[3], parv[4]);
}

/*
 * me_swebirc
 *      parv[1] = operation (req, ack, nak)
 *      parv[2] = client uid
 *      parv[3] = client realhost
 *      parv[4] = client sockhost
 *      parv[5] = client certfp
 *      parv[6] = webirc certfp
 *      parv[7] = webirc password
 *      parv[8] = webirc username
 *      parv[9] = requested host
 *      parv[10] = requsted ip
 */
static void
me_swebirc(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  const char *operation = parv[1],
             *uid = parv[2],
             *sockhost = parv[4],
             *password = parv[parc - 4],
             *host = parv[parc - 2],
             *ip = parv[parc - 1];

  /* update cache whether or not the client still exists */
  if (!strcmp(operation, "ACK"))
  {
    webirc_cache_add(sockhost, password);
  }
  else if (!strcmp(operation, "NAK"))
  {
    webirc_cache_del(sockhost, password);
  }

  struct Client *target_p = hash_find_id(uid);
  if (target_p == NULL || !IsUnknown(target_p) || !(target_p->localClient->registration & REG_NEED_WEBIRC))
    return;

  // only looking for successful operations
  if (!strcmp(operation, "ACK"))
  {
    webirc_apply(target_p, host, ip);

    target_p->localClient->registration &= ~REG_NEED_WEBIRC;
    if (!target_p->localClient->registration)
      register_local_user(target_p);
  }
  else if (!strcmp(operation, "NAK"))
  {
    sendto_one(target_p, ":%s NOTICE %s :WEBIRC: access denied (no access, invalid password, or user limit exceeded)",
               me.name, target_p->name[0] ? target_p->name : "*");
    exit_client(target_p, &me, "WEBIRC: access denied");
  }
}

static struct Message webirc_msgtab =
{
  .cmd = "WEBIRC",
  .args_min = 5,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = mr_webirc,
  .handlers[CLIENT_HANDLER] = m_registered,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_registered,
};

static struct Message swebirc_msgtab =
{
  .cmd = "SWEBIRC",
  .args_min = 11,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_swebirc,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&webirc_msgtab);
  mod_add_cmd(&swebirc_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&webirc_msgtab);
  mod_del_cmd(&swebirc_msgtab);

  webirc_cache_clear();
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
