#include "plexus_test.h"

#define READBUF_SIZE 16384

static char readBuf[READBUF_SIZE];

void expect(struct PlexusClient *client, const char *str)
{
  for (;;)
  {
    io_loop();

    for (;;)
    {
      int length = extract_one_line(&client->buf_recvq, readBuf);
      if (length == 0)
        break;

      //printf("IN %s\n", readBuf);

      if (match(str, readBuf))
        continue;

      return;
    }
  }
}

void expect_numeric(struct PlexusClient *client, enum irc_numerics numeric)
{
  char buffer[IRCD_BUFSIZE];

  snprintf(buffer, sizeof(buffer), ":%s %03d %s *", me.name, numeric, client->name);

  expect(client, buffer);
}

void expect_message(struct PlexusClient *client, struct Client *from, const char *message)
{
  char buffer[IRCD_BUFSIZE];

  snprintf(buffer, sizeof(buffer), ":%s!%s@%s %s *", from->name, from->username, from->host, message);

  expect(client, buffer);
}
