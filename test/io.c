#include "plexus_test.h"

static void io_send(struct PlexusClient *client, struct dbuf_block *block)
{
  int i = send(client->fd.fd, block->data, block->size, 0);
  //printf("OUT %s", block->data);
  assert(i == block->size);
}

void io_write(struct PlexusClient *client, const char *buf, ...)
{
  struct dbuf_block *block = dbuf_alloc();
  va_list args;

  va_start(args, buf);
  dbuf_put_args(block, buf, args);
  va_end(args);

  if(block->size > IRCD_BUFSIZE - 2)
    block->size = IRCD_BUFSIZE - 2;

  block->data[block->size++] = '\r';
  block->data[block->size++] = '\n';
  block->data[block->size] = 0;

  io_send(client, block);

  dbuf_ref_free(block);
}

void io_read(fde_t *fd, struct PlexusClient *client)
{
#define READBUF_SIZE 16384
  static char readBuf[READBUF_SIZE];

  int length = recv(fd->fd, readBuf, READBUF_SIZE, 0);

  if (length <= 0)
  {
    if (length < 0 && ignoreErrno(errno))
    {
      comm_setselect(fd, COMM_SELECT_READ, (PF *) io_read, client);
      return;
    }

    /* what do here? */
    ck_abort();
//    dead_link_on_read(client_p, length);
    return;
  }

  dbuf_put(&client->buf_recvq, readBuf, length);

  comm_setselect(fd, COMM_SELECT_READ, (PF *) io_read, client);
}

