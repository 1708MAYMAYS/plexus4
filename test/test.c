#include "plexus_test.h"

#include <jansson.h>
#include <setjmp.h>

/* symbols imported by libplexus for upgrade stuff */
json_t *plexus_upgrade;
jmp_buf plexus_upgrade_jmp;

static void add_testcases(Suite *s)
{
  invite_setup(s);
}

int main()
{
  Suite *s = suite_create("plexus");

  add_testcases(s);

  SRunner *runner = srunner_create(s);
  srunner_set_tap(runner, "-");
  srunner_run_all(runner, CK_NORMAL);

  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
