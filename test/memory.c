#include <stdlib.h>
#include <stddef.h>
#include <string.h>

/* alloc functions imported by libplexus for upgrade stuff */

void *ircd_alloc(size_t s);
void *ircd_realloc(void *s, size_t size);
void ircd_free(void *s);

struct chunk
{
  size_t sz;
  char memory[];
};

void *ircd_alloc(size_t s)
{
  struct chunk *c = calloc(1, sizeof(struct chunk) + s);
  if (c == NULL)
    return NULL;

  c->sz = s;

  return c->memory;
}

void *ircd_realloc(void *s, size_t size)
{
  if (!s)
    return ircd_alloc(size);

  if (!size)
  {
    ircd_free(s);
    return NULL;
  }

  struct chunk *c = (struct chunk *) (((char *) s) - sizeof(struct chunk));

  c = realloc(c, size);

  if (c == NULL)
    return NULL;

  c->sz = size;

  return c->memory;
}

void ircd_free(void *s)
{
  struct chunk *c = (struct chunk *) (((char *) s) - sizeof(struct chunk));

  memset(c->memory, 0xaa, c->sz);
  free(c);
}

