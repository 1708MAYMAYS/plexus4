#include "stdinc.h"
#include "listener.h"
#include "s_bsd.h"
#include "memory.h"
#include "client.h"
#include "irc_string.h"
#include "packet.h"
#include "numeric.h"
#include "hash.h"
#include "ircd.h"
#include "restart.h"
#include "conf.h"
#include "s_serv.h"
#include "channel_mode.h"

#include <check.h>

#define WAIT_FOR(cond) while (!(cond)) io_loop()

struct PlexusClient
{
  struct Client *client;

  char name[HOSTLEN + 1]; /**< unique name for a client nick or host */
  fde_t         fd;
  //struct dbuf_queue buf_sendq;
  struct dbuf_queue buf_recvq;
};

extern void plexus_up(void);
extern void plexus_up_conf(const char *);
extern void plexus_down(void);

extern void io_write(struct PlexusClient *client, const char *buf, ...);
extern void io_read(fde_t *fd, struct PlexusClient *client);

extern struct PlexusClient *client_create(const char *);
extern struct PlexusClient *client_register(const char *);

extern void expect(struct PlexusClient *client, const char *str);
extern void expect_numeric(struct PlexusClient *client, enum irc_numerics numeric);
extern void expect_message(struct PlexusClient *client, struct Client *from, const char *message);

extern void irc_join(struct PlexusClient *client, const char *channel);
extern void irc_invite(struct PlexusClient *from, struct Channel *chptr, struct Client *to);

extern void plexus_fork(void (*callback)());
extern bool plexus_branch();
extern void plexus_join_all_and_exit();

extern void invite_setup(Suite *);

