#include "plexus_test.h"

static struct PlexusClient *create(const char *name, struct irc_ssaddr *ip)
{
  int fds[2];

  if (socketpair(AF_UNIX, SOCK_STREAM, AF_UNSPEC, fds))
  {
    ck_abort_msg("socketpair failed");
    return NULL;
  }

  struct PlexusClient *pclient = MyMalloc(sizeof(struct PlexusClient));
  struct Listener *listener = find_listener_flags(0);
  struct Client *client = add_connection(listener, ip, fds[0]);

  pclient->client = client;
  strlcpy(pclient->name, name, sizeof(pclient->name));
  fd_open(&pclient->fd, fds[1], 1, "plexus client");

  comm_setselect(&pclient->fd, COMM_SELECT_READ, (PF *) io_read, pclient);

  return pclient;
}

struct PlexusClient *client_create(const char *name)
{
  union
  {
    struct irc_ssaddr ss;
    struct sockaddr_in in;
  } u;

  memset(&u, 0, sizeof(u));

  u.ss.ss.ss_family = AF_INET;
  inet_pton(AF_INET, "127.0.0.1", &u.in.sin_addr);

  fix_irc_ssaddr(&u.ss);

  return create(name, &u.ss);
}

struct PlexusClient *client_register(const char *name)
{
  struct PlexusClient *client = client_create(name);

  io_write(client, "USER %s . . :%s", client->name, client->name);
  io_write(client, "NICK %s", client->name);

  expect_numeric(client, RPL_WELCOME);

  return client;
}

