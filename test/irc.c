#include "plexus_test.h"

void irc_join(struct PlexusClient *client, const char *channel)
{
  io_write(client, "JOIN %s", channel);
}

void irc_invite(struct PlexusClient *from, struct Channel *chptr, struct Client *to)
{
  io_write(from, "INVITE %s %s", to->name, chptr->chname);
}

