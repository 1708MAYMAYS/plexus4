#include "plexus_test.h"

extern int plexus_main(int, char*[]);

void plexus_up(void)
{
  plexus_up_conf(RESOURCEDIR "/test.conf");
}

void plexus_up_conf(const char *configfile)
{
  char pidfile[PATH_MAX];

  snprintf(pidfile, sizeof(pidfile), "%s.%d", PPATH, getpid());

  char *args[] = {
    SPATH,
    "-configfile",
    (char *) configfile,
    "-pidfile",
    pidfile,
    "-foreground",
    "-test"
  };

  plexus_main(sizeof(args) / sizeof(*args), args);
}

void plexus_down(void)
{
  plexus_join_all_and_exit();
  server_die("End of test", 0); // this will exit
}

