#include "plexus_test.h"

#define MAX_PROCS 32

static pid_t pids[MAX_PROCS];

static void add_process(pid_t pid)
{
  for (int i = 0; i < MAX_PROCS; ++i)
  {
    if (!pids[i])
    {
      pids[i] = pid;
      return;
    }
  }

  // check will kill pid
  ck_abort_msg("out of process space");
}

static pid_t fork_process()
{
  pid_t pid = check_fork();

  ck_assert(pid >= 0);

  if (pid > 0)
  {
    // parent.
    add_process(pid);
  }
  else
  {
    // child.
    memset(pids, 0, sizeof(pids)); // child has no subprocesses
  }

  return pid;
}

void plexus_fork(void (*callback)())
{
  if (!plexus_branch())
  {
    callback();
    exit(EXIT_SUCCESS);
  }
}

bool plexus_branch()
{
  pid_t pid = fork_process();

  return pid > 0;
}

/* lifted from check */
static int waserror(int status, int signal_expected)
{
    int was_sig = WIFSIGNALED(status);
    int was_exit = WIFEXITED(status);
    int exit_status = WEXITSTATUS(status);
    int signal_received = WTERMSIG(status);

    return ((was_sig && (signal_received != signal_expected)) ||
            (was_exit && exit_status != 0));
}

void plexus_join_all_and_exit()
{
  for (int i = 0; i < MAX_PROCS; ++i)
  {
    if (!pids[i])
      continue;

    int status;
    pid_t pid;

    do
    {
      pid = waitpid(pids[i], &status, 0);
    }
    while (pid == -1);

    if (waserror(status, 0))
    {
      exit(EXIT_FAILURE);
    }
  }

  exit(EXIT_SUCCESS);
}

