/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file channel_mode.h
 * \brief Includes channel mode related definitions and structures.
 * \version $Id$
 */

#ifndef INCLUDED_channel_mode_h
#define INCLUDED_channel_mode_h

#define MODEBUFLEN    200

/* Maximum mode changes allowed per client, per server is different */
#define MAXMODEPARAMS 4

enum
{
  MODE_QUERY =  0,
  MODE_ADD   =  1,
  MODE_DEL   = -1
};

enum
{
  CHACCESS_NOTONCHAN = -1,
  CHACCESS_PEON      =  0,
  CHACCESS_HALFOP    =  1,
  CHACCESS_CHANOP    =  2,
  CHACCESS_PROTECTED =  3,
  CHACCESS_OWNER     =  4,
  CHACCESS_REMOTE    =  5
};

/* can_send results */
enum
{
  CAN_SEND_NO    =  0,
  CAN_SEND_NONOP = -1,
  CAN_SEND_OPV   = -2
};


/* Channel related flags */
enum
{
  CHFL_OWNER        = 0x0001,  /* Channel owner */
  CHFL_PROTECTED    = 0x0002,  /* Protected user */
  CHFL_CHANOP       = 0x0004, /* Channel operator   */
  CHFL_HALFOP       = 0x0008, /* Channel half op    */
  CHFL_VOICE        = 0x0010, /* the power to speak */
  CHFL_DEOPPED      = 0x0020, /* deopped by us, modes need to be bounced */
  CHFL_BAN          = 0x0040, /* ban channel flag */
  CHFL_EXCEPTION    = 0x0080, /* exception to ban channel flag */
  CHFL_INVEX        = 0x0100,

  /* cache flags for silence on ban */
  CHFL_BAN_CHECKED  = 0x0200,
  CHFL_BAN_SILENCED = 0x0400,
  CHFL_MUTE_CHECKED = 0x0800
};

/* channel modes ONLY */
#define MODE_PRIVATE    0x0001
#define MODE_SECRET     0x0002
#define MODE_MODERATED  0x0004
#define MODE_TOPICLIMIT 0x0008
#define MODE_INVITEONLY 0x0010
#define MODE_NOPRIVMSGS 0x0020
#define MODE_SSLONLY    0x0040
#define MODE_OPERONLY   0x0080
#define MODE_REGONLY    0x0200
#define MODE_NOCTRL     0x0400
#define MODE_MODREG     0x0800
#define MODE_BWSAVER    0x1000
#define MODE_NOCTCP     0x2000
#define MODE_NONOTICES  0x4000
#define MODE_PERSIST    0x8000

/* name invisible */
#define SecretChannel(x)        (((x)->mode.mode & MODE_SECRET))
#define PubChannel(x)           (!SecretChannel(x))
/* knock is forbidden, halfops can't kick/deop other halfops.
 * +pi means paranoid and will generate notices on each invite */
#define PrivateChannel(x)       (((x)->mode.mode & MODE_PRIVATE))
#define BwSaverChannel(x)       (((x)->mode.mode & MODE_BWSAVER))
#define PersistChannel(x)       (((x)->mode.mode & MODE_PERSIST))

#ifdef CHANAQ
# ifdef HALFOP
#  define PREFIX_NUM 5
# else
#  define PREFIX_NUM 4
# endif
#else
# ifdef HALFOP
#  define PREFIX_NUM 3
# else
#  define PREFIX_NUM 2
# endif
#endif

#define CHANOWNER_PREFIX '~'
#define CHANPROTECT_PREFIX '&'

struct ChModeChange
{
  char letter;
  const char *arg;
  const char *id;
  int dir;
  unsigned int caps;
  unsigned int nocaps;
  int mems;
  struct Client *client;
};

struct ChCapCombo
{
  int count;
  unsigned int cap_yes;
  unsigned int cap_no;
};

struct ChannelMode
{
  void (*func)(struct Client *client_p, struct Client *source_p,
               struct Channel *chptr, int parc, int *parn,
               char **parv, int *errors, int alev, int dir, char c, unsigned int d);
  unsigned int mode;
  char letter;
};

extern struct ChannelMode mode_table[];

extern struct ChannelMode *get_channel_mode(char c);

extern struct Ban *add_id(struct Client *, struct Channel *, const char *, unsigned int);
extern const char *get_mask(struct Ban *ban);
extern void set_channel_mode(struct Client *, struct Client *, struct Channel *,
                             struct Membership *, int, char **);
extern void clear_ban_cache(struct Channel *);
extern void clear_ban_cache_client(struct Client *);
extern void init_chcap_usage_counts(void);
extern void set_chcap_usage_counts(struct Client *);
extern void unset_chcap_usage_counts(struct Client *);
#endif /* INCLUDED_channel_mode_h */
