/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "ircd_defs.h"
#include "list.h"
#include "snomask.h"
#include "irc_string.h"

static dlink_list snomasks;

struct Snomask sno_cconn = {
  .mode = 'c',
  .remote = 1
};

struct Snomask sno_rej = {
  .mode = 'j'
};

struct Snomask sno_skill = {
  .mode = 'k'
};

struct Snomask sno_full = {
  .mode = 'f'
};

struct Snomask sno_spy = {
  .mode = 'y'
};

struct Snomask sno_debug = {
  .mode = 'd'
};

struct Snomask sno_nchange = {
  .mode = 'n'
};

struct Snomask sno_bots = {
  .mode = 'b',
  .remote = 1
};

struct Snomask sno_link = {
  .mode = 'l',
  .remote = 1
};

struct Snomask sno_unauth = {
  .mode = 'u'
};

static unsigned int
find_mask()
{
  dlink_node *ptr;
  unsigned int used = 0;
  unsigned int i;

  DLINK_FOREACH(ptr, snomasks.head)
  {
    struct Snomask *s = ptr->data;
    used |= s->flag;
  }

  for (i = 1; i < SNOMASK_MASK && used & i; i <<= 1);

  return i;
}

void
snomask_init()
{
  snomask_add(&sno_cconn);
  snomask_add(&sno_rej);
  snomask_add(&sno_skill);
  snomask_add(&sno_full);
  snomask_add(&sno_spy);
  snomask_add(&sno_debug);
  snomask_add(&sno_nchange);
  snomask_add(&sno_bots);
  snomask_add(&sno_link);
  snomask_add(&sno_unauth);
}

void
snomask_add(struct Snomask *sno)
{
  unsigned int mask = find_mask();
  if (!mask)
    return;

  sno->flag = mask;
  dlinkAdd(sno, &sno->node, &snomasks);
}

void
snomask_del(struct Snomask *sno)
{
  if (!sno->flag)
    return;

  dlinkDelete(&sno->node, &snomasks);
}

struct Snomask *
snomask_find(char c)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, snomasks.head)
  {
    struct Snomask *s = ptr->data;

    if (ToLower(c) == ToLower(s->mode))
      return s;
  }

  return NULL;
}

const char *
snomask_to_str(struct Snomasks *mask)
{
  static char buf[IRCD_BUFSIZE];
  char *p = buf;
  dlink_node *ptr;

  *p++ = '+';

  DLINK_FOREACH(ptr, snomasks.head)
  {
    struct Snomask *s = ptr->data;

    if (mask->local & s->flag)
      *p++ = ToLower(s->mode);
    if (mask->remote & s->flag)
      *p++ = ToUpper(s->mode);
  }

  *p++ = 0;

  return buf;
}

void
snomask_parse(const char *mask, struct Snomasks *add, struct Snomasks *remove)
{
  int doadd = 1;

  add->local = add->remote = 0;
  remove->local = remove->remote = 0;

  for (const char *p = mask; *p; ++p)
    switch (*p)
    {
      case '+':
        doadd = 1;
        break;
      case '-':
        doadd = 0;
        break;
      default:
      {
        int is_remote = IsUpper(*p);
        struct Snomask *s = snomask_find(*p);
        if (s)
        {
          if (doadd)
          {
            if (is_remote && !s->remote)
              continue;

            if (is_remote)
            {
              add->remote |= s->flag;
              remove->remote &= ~s->flag;
            }
            else
            {
              add->local |= s->flag;
              remove->local &= ~s->flag;
            }
          }
          else
          {
            if (is_remote)
            {
              remove->remote |= s->flag;
              add->remote &= ~s->flag;
            }
            else
            {
              remove->local |= s->flag;
              add->local &= ~s->flag;
            }
         }
        }
        break;
      }
    }
}

void
snomask_set(struct Snomasks *masks, struct Snomasks *add, struct Snomasks *remove)
{
  if (add)
  {
    masks->local |= add->local;
    masks->remote |= add->remote;
  }

  if (remove)
  {
    masks->local &= ~remove->local;
    masks->remote &= ~remove->remote;
  }
}

int
snomask_has(struct Snomasks *masks, struct Snomask *snomask, int remote)
{
  if (remote)
    return masks->remote & snomask->flag;
  else
    return masks->local & snomask->flag;
}

